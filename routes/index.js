var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { page: 'tienda retro' });
});
router.get('/acerca_de', function(req, res, next) {
  res.render('acerca_de', { page: 'acerca de' });
});

module.exports = router;
